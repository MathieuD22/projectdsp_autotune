// Processing functions
#include "processing.h"
#include "fixFFT/fixFFT.h"
#include <stdint.h>
#include <unistd.h>
#include <complex.h>

void benchmark(int16_t* sig_out,int16_t* sig,int64_t size){
   //TODO Iteration 4: You will use this function, but nothing has to be modified in the function
    int mc_runs = 1;
    struct_timer timer;
    /*--------------------------------------------------------------------------
     * --- Main benchmark call (Monte carlo)
     * --------------------------------------------------------------------------*/
    printf("Starting routine\n");
    timer = tic();
    for (int c = 0 ; c < mc_runs ; c++){
        processing(sig_out,sig,size);
    }
    timer = toc(timer);
    printf("output\n");
    print_toc(timer);
}


int processing(int16_t* sig_out,int16_t* sig, int64_t size)
{
   //TODO Iteration 2: This should fill the function to synthetize sig_out, a new signal (independant from sig)
   //TODO Iteration 3: This should fill the function to synthetize a sig_out, new signal based from sig

	int liste_freq_notes[]={16, 17, 18, 19, 21, 22, 23, 24, 26, 27, 29, 31};//first notes
	//add other notes
	int liste_freq_notes2[108];
	for (int i=0;i<=8;i++){
		for (int k=0;k<=11;k++){
			liste_freq_notes2[i*12+k]=liste_freq_notes[k]<<i;
			printf("listfreq[%d] = %d\n",i*12+k,liste_freq_notes2[i*12+k]);
		}
	}



	int16_t partiereelle[size];
	int16_t partieimaginaire[size];
	int16_t module_carre[size];
	//calculation of real and imaginary parts
	for (int i=0;i<size;i++){
		partiereelle[i]=creal(sig[i]);
		partieimaginaire[i]=cimag(sig[i]);
	}

	int t =fix_fft(partiereelle,partieimaginaire,10,0);//FFT
	int max=0;
	int f=0;
	//We seek the maximum value of the modulus
	for (int i=0;i<size;i++){
		module_carre[i]=mul_int16_int16(partiereelle[i],partiereelle[i])+mul_int16_int16(partieimaginaire[i],partieimaginaire[i]);
		if(module_carre[i]>max){
			max=module_carre[i];
			f=i;// f is the index where the modulus value is the highest
		}
	}
	printf("i = %d\n",f);
	f=f*8000/1024/2;//frequency=f*FS/SIZE_FFT/2
	int freq_note1;
	int freq_note2;
	int i=1;
	//Here we are looking between which value of the list of notes is included the frequency
	while (f>liste_freq_notes2[i]){
	    freq_note1=liste_freq_notes2[i];
	    freq_note2=liste_freq_notes2[i+1];
	    i=i+1;
	}

	//Now we search the nearest value
	int freq_note;
	if (freq_note2-liste_freq_notes2[i-1]<liste_freq_notes2[i-1]-freq_note1){
	    freq_note=freq_note2;
	}
	else {
	    freq_note=freq_note1;
	}

	printf("max = %d\n",max);
	printf("f= %d\n",f);
	printf("note : %d\n",freq_note);

	//sig_out=fpsin();

   //TODO Iteration 4: This should update this function to synthetize a signal sig_out, with part of sig
    return 0;
}




/* We use halfword (Int16_t) and with classic product it takes LSB and not MSB
 * => Ensure double precision multiplier and shift to take MSB
 */
inline int16_t mul_int16_int16(int16_t x,int16_t y)
{
    return  (int32_t)(x *  y) >> 16;
}

int16_t fpsin(int16_t x)
{
   //TODO: Iteration 2: Let's implement a sine in Fixed Point domain !
   int16_t a = 25718; // Q(16,1,14)
   int16_t b = -20953; // Q(16,0,15)
   int16_t c = 18276; // Q(16,-3,18)

   int16_t XbarreP2 = mul_int16_int16(x,x)<<1; // Q(16,0,15)
   int16_t XbarreP3 = mul_int16_int16(x,XbarreP2)<<1; // Q(16,0,15)
   int16_t XbarreP5 = mul_int16_int16(XbarreP2,XbarreP3)<<1; // Q(16,0,15)

   Axbarre = mul_int16_int16(a,x); // Q(16,2,13) common format
   Bxbarre = mul_int16_int16(b,XbarreP3)>>1; // Q(16,2,13)
   Cxbarre = mul_int16_int16(c,XbarreP5)>>4; // Q(16,2,13)

   ADD1 = Axbarre + Bxbarre;
   return (int16_t)((ADD1 + Cxbarre)<<2);  // output format Q(16,0,15)
}

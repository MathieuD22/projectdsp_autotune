% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.

%clearvars all 
%close all
%clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    synthetizepitch.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [s] = synthetizepitch(y)
maxsignal = y;
TableFreq = [16.35,17.32,18.35,19.45,20.60,21.83,23.12,24.50,25.96,27.50,29.14,30.87];
NoteFreq = zeros(1,8*length(TableFreq));    %C minor table
min = 1000;                                  % initialize min to detec the pitch frequency
FREQUENCE = 0;
for i=0:1:8
    for j=1:1:length(TableFreq)
        NoteFreq(i*length(TableFreq)+j)=TableFreq(j)*(2^i);
        soustraction = abs(NoteFreq(i*length(TableFreq)+j)-maxsignal);
        if(min>soustraction)
            min = soustraction;
            FREQUENCE=NoteFreq(i*length(TableFreq)+j);     % the pitch frequency
        end
    end
end
matricef = reshape(NoteFreq,12,[]);             % (optionnal) put the C minor table into a matrice to organize frequencies

s = FREQUENCE;

end



% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.

clearvars all 
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    generatesinus.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure(1)
x = -pi/2:0.01:pi/2;
signal = sin(x);
plot(x, signal)
max = pitchdetection(signal);
max

%tfourier = abs(fftshift(signal));
%[Amplitude, iFreq] = max(tfourier);

%disp(maxsignal)
%synth = synthetizepitch(signal);
%figure
%plot(synth)

%maxsignal2 = pitchdetection(synth);

%% Pitch Shift using the different sweep files

%% synth_beep_1.wav

[sig,fs] = audioread("/users/elo/mduplan/Processeur_Traitement_du_signal/2a_sysnum_dsp_project_autotune/Base_Sound/synth_beep_1.wav");

DecomposeSignal = round(length(sig)/1024);
matriceSIG = reshape((sig(1:1:DecomposeSignal*1024)),1024,DecomposeSignal);
maxsignalAudio = zeros(1,DecomposeSignal+1);
SynthetizeAudio = zeros(DecomposeSignal,40000);
T=1/8000;
s = zeros(1,40000);
for i=1:1:DecomposeSignal
    maxsignalAudio(i) = pitchdetection(sig(1+(i-1)*1024:i*1024));
    Debut = (i-1)*1024+1;
    Fin = i*1024;
    SynthetizeAudio = synthetizepitch(maxsignalAudio(1,i));
    for k=Debut:1:Fin
        s(k) = sin(2*pi*SynthetizeAudio*(T*k));
    end
end
maxsignalAudio(DecomposeSignal+1) = pitchdetection(sig(DecomposeSignal*1024:length(sig)));
SynthetizeAudio = synthetizepitch(maxsignalAudio(DecomposeSignal+1));
for k=DecomposeSignal*1024+1:1:length(sig)
    s(k) = sin(2*pi*SynthetizeAudio*(T*k));
end
MatriceTemps = reshape(s(1:length(s)),[],100);
MatriceTemps = MatriceTemps.*blackman(400);
MatriceFrequence = abs(fft(MatriceTemps));

figure(2)
AfficheSignalTempsFrequence(8000,s)
figure(2)
title("TF grid of synth beep 1");
hold on;

%% synth_sweep_1.wav

[SigSweep,FsSweep] = audioread("/users/elo/mduplan/Processeur_Traitement_du_signal/2a_sysnum_dsp_project_autotune/Base_Sound/synth_sweep_1.wav");

DecomposeSignal = round(length(SigSweep)/1024);
matriceSIG = reshape((SigSweep(1:1:DecomposeSignal*1024)),1024,DecomposeSignal);
maxsignalAudio = zeros(1,DecomposeSignal+1);
T=1/8000;
s = zeros(1,40000);
for i=1:1:DecomposeSignal
    maxsignalAudio(i) = pitchdetection(SigSweep(1+(i-1)*1024:i*1024));
    Debut = (i-1)*1024+1;
    Fin = i*1024;
    SynthetizeAudio = synthetizepitch(maxsignalAudio(1,i));
    for k=Debut:1:Fin
        s(k) = sin(2*pi*SynthetizeAudio*(T*k));
    end
end
maxsignalAudio(DecomposeSignal+1) = pitchdetection(SigSweep(DecomposeSignal*1024:length(SigSweep)));
SynthetizeAudio = synthetizepitch(maxsignalAudio(DecomposeSignal+1));
for k=DecomposeSignal*1024+1:1:length(SigSweep)
    s(k) = sin(2*pi*SynthetizeAudio*(T*k));
end
MatriceTemps = reshape(s(1:length(s)),[],100);
MatriceTemps = MatriceTemps.*blackman(400);
MatriceFrequence = abs(fft(MatriceTemps));

figure(3)
AfficheSignalTempsFrequence(8000,s)
figure(3)
title("TF grid of synth sweep 1");
hold on;




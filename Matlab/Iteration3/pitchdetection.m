% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.

%clearvars all 
%close all
%clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    pitchdetection.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function maxy =pitchdetection(signal)
    y = xcorr(signal);
    maxy = max(y);
end






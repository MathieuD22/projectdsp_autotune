% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.

%clearvars all 
%close all
%clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    FPsine2.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [res] = FPsine2(signal, FreqEch, duree)

res = zeros(1,FreqEch*duree);
a = shift_left(FixCode(4*(3/pi-9/16),14),15);       %Q(16,1,14)
b = shift_left(FixCode(-2*a+5/2,15),15);            %Q(16,0,15)
c = shift_left(FixCode(a-3/2,15),15);               %Q(16,0,15)
for i=1:1:(FreqEch*duree)
    xbarre = shift_left(FixCode((2*signal(i)/pi),14),14); %Q(16,0,15) << Q(16,1,14) 
    XbarreP2 = shift_right(multiplier(xbarre,xbarre),1); %Q(16,0,15)
    XbarreP3 = shift_right(multiplier(XbarreP2,xbarre),1); %Q(16,0,15)
    XbarreP5 = shift_right(multiplier(XbarreP3,XbarreP2),1); %Q(16,0,15)
    Axbarre = shift_right(multiplier(shift_right(a,1),xbarre),1); % Q(16,0,15)
    Bxbarre = shift_right(multiplier(b,XbarreP3),1);
    Cxbarre = shift_right(multiplier(c,XbarreP5),1);
    ADD1 = shift_right(add_int16_int16(Axbarre,Bxbarre),1);
    res(i) = add_int16_int16(ADD1,Cxbarre);
%for x=-pi:0.01:pi
    %if(x<-pi/2 && x>=-pi)
        %signal(round((Fin+x)*100)) = -sin(pi+x);
    %end
    %if(x>pi/2 && x<=pi)
        %signal(round((Fin+x)*100)) = sin(pi-x);
    %else
        %signal(round((Fin+x)*100)) = sin(x);
    %end
    
%end
end
end
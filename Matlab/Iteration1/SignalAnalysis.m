% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.

clearvars all 
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    LOAD FILE guitar_1.wav

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%sig%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%addpath('Z:\TraitementNumeriqueduSignal\2a_sysnum_dsp_lab_filtering_on_zybo-master\Matlab\Fonctions\');
addpath('/users/elo/mduplan/Processeur_Traitement_du_signal/2a_sysnum_dsp_project_autotune/Base_Sound')

[sig,fs] = audioread("guitar_1.wav");
info = audioinfo("guitar_1.wav");

T = (length(sig)-1)/fs;
K=100;
duration = info.Duration*0.01;
NombrePoint = duration * fs;
%NbSeg


%% the signal guitar_1.wav in frequency domain
figure(1); 
D = abs(fftshift(fft(sig)));
plot((0:length(sig)-1)*(fs/length(sig))/2,20*log10(D));
title('Fréquence du signal');
xlabel("Fréquence en Hz");
ylabel("Amplitude"); 
hold on;

%% the signal guitar_1.wav in time domain
figure(2);
plot(sig);
title('Temps du signal');
xlabel("Temps en s");
ylabel("Amplitude"); 
hold on;

%% the signal guitar_1.wav in time / frequency domain
figure(3)
apodis = blackman(NombrePoint);           % Création de la fenêtre d'apodisation
matricet = reshape(sig(1:length(sig)),[],K);
matricet = apodis.*matricet;
Modulematricef = 20*log10(abs(fft(matricet)));
Anglematricef = angle(fft(matricet));

%%% Module
rangef1 = (0:length(sig)-1)*(fs/length(sig));     %frequency range
imagesc((0:5),rangef1/2,Modulematricef)
set(gca,"YDir",'normal')
title("Module of the TF grid for the file guitar_1.wav");
xlabel("Temps en s");
ylabel("Fréquence en Hz"); 
hold on;

%%% Angle

figure(4)
imagesc(Anglematricef)
set(gca,"YDir",'normal')
title("Module of the TF grid for the file guitar_1.wav");
xlabel("Temps en s");
ylabel("Fréquence en Hz"); 
hold on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    LOAD FILE synth_sweep_1.wav

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[sigsynth,fssynth] = audioread("synth_sweep_1.wav");
infoSynth = audioinfo("synth_sweep_1.wav");
Tsynth = (length(sig)-1)/fssynth;
K=100;
durationSynth = infoSynth.Duration*0.01;
NombrePointSynth = durationSynth * fs;

%% the signal synth_sweep_1.wav in frequency domain

figure(5); 
Dsynth = abs(fftshift(sigsynth));
plot((0:length(sigsynth)-1)*(fssynth/length(sigsynth))/2,20*log10(Dsynth));
title("Fréquence du signal");
xlabel("Fréquence en Hz");
ylabel("Amplitude"); 
hold on;

%% the signal synth_sweep_1.wav in time domain
figure(6);
plot(sigsynth);
title('Temps du signal');
xlabel("Temps en s");
ylabel("Amplitude"); 
hold on;

%% the signal synth_sweep_1.wav in time / frequency domain

figure(7)
apodis = blackman(400);                    % Création de la fenêtre d'apodisation
matricet = reshape((sigsynth(1:length(sigsynth))),[],K);
Matricet = apodis.*matricet;
Modulematricef = 20*log10(abs(fft(Matricet)));          % Matrice fréquencielle 
Anglematricef = (angle(fft(Matricet)));         % Matrice fréquencielle 

%%% Module
rangef = (0:length(sigsynth)-1)*(fssynth/length(sigsynth));     %frequency range
imagesc((0:5),rangef/2,Modulematricef(1:end/2,:))
set(gca,"YDir",'normal')
title("Module of the TF grid for the file synthsweep1.wav");
xlabel("Temps en s");
ylabel("Fréquence en Hz"); 
hold on;

%%% Angle
figure(8)
imagesc((0:5),(0:length(Anglematricef)),Anglematricef(1:end/2,:))
set(gca,"YDir",'normal')
title("Angle of the TF grid for the file synthsweep1.wav");
xlabel("Temps en s");
ylabel("Fréquence en Hz"); 
hold on;

%% Active Frequency
Correlation = xcorr(sigsynth);
figure(9)
plot(Correlation)
MaxCorrelation = max(Correlation);
MaxCorrelation
M = max(Modulematricef);                              % On récupère la fréquence maximale de chaque colonne de notre matrice
PositionTempActiveFreq = abs(ifft(10.^(M/20)));

total = 0;
for i = 2:numel(PositionTempActiveFreq)
    total = total + PositionTempActiveFreq(i);  % Moyenne de temps pour obtenir le Temps correspondant à la fréquence maximale du signal
end

PositionTempMaxFreq = total/(length(PositionTempActiveFreq)-1);
disp(PositionTempMaxFreq)
ActiveFreq = max(M);
disp(ActiveFreq)




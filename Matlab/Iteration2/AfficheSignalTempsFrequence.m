% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    AfficheSignalTempsFrequence.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function AfficheSignalTempsFrequence(FreqEch, Signal)
   
    apodis = blackman(length(Signal)/100);       % Création de la fenêtre d'apodisation
    matricet = reshape((Signal(1:1:length(Signal))),[],100);
    MatricetApodis = apodis.*matricet;
    matricef =(abs(fft(MatricetApodis)));        % Matrice fréqencielle 
    
    figure(2)
    AxeX = 0:5;
    AxeY = 0:FreqEch/2;
    imagesc(AxeX, AxeY, matricef(1:end/2,:))
    colorbar
    colormap("default")
    set(gca,"YDir",'normal')
    title("T/F grid of the second signal to synthetize");
    xlabel("Time [s]");
    ylabel("Frequency [Hz]"); 
    hold on;
    
end


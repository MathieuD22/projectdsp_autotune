% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.

clearvars all 
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    generate_synth_sig_2.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Second Signal to reconstrut

signal = zeros(1,40000);

Sig1 = GenerationSignal(0,0.7,300);
Sig2 = GenerationSignal(0,2.5,400);
Sig3 = GenerationSignal(0.6,0.9,100);
Sig4 = GenerationSignal(0.9,2.9,700);
Sig5 = GenerationSignal(2.4,5,1000);
Sig6 = GenerationSignal(4.5,5,500);
Sig7 = GenerationSignal(2.9,4.5,2400);

for i=1:1:40000
    signal(i) = Sig1(i) + Sig2(i) + Sig3(i) + Sig4(i) + Sig5(i) + Sig6(i) + Sig7(i);
end

AfficheSignalTempsFrequence(8000,signal)

%% Real sine

x = -pi/2:0.01:pi/2;
signal = sin(x);
%plot
figure(3)
plot(x, signal)
title("The real sine");
xlabel("x en radian")
ylabel("Amplitude")
hold on

%% FixSine we use the FPsine function

x = -pi/2:0.01:pi/2;
signalFixC = zeros(1,315);
for i=1:1:315
    if(x(i)<(pi/2) || x(i)>(-pi/2)) % we take x belong ]-pi/2;pi/2[
        signalFixC(i) = FPsine(x(i));
    end
end
signalFixC = signalFixC .* power(2,-15);
% Plot
figure(4)
plot(x, signalFixC)
title("The fixed point calculation")
xlabel("x en radian")
ylabel("Amplitude")
hold on

%% Taylor Sine

y = fixSine(x,105,3);

figure(5)
plot(x, y)
title("The floating point approximation")
xlabel("x en radian")
ylabel("Amplitude")
hold on

%% Plotting all signals

figure(6)
plot(x,signal, x,y,x,signalFixC)
title("The floating point approximation")
xlabel("x en radian")
ylabel("Amplitude")
hold on

a = 4*(3/pi-9/16);
a1 = FixCode(a,14).*power(2,14);
a1
b = -2*a+5/2;
b1 = FixCode(b,15).*power(2,15);
c = a-3/2;
c1 = FixCode(c,18).*power(2,18);
c1
FreqEchhehhfb = FixCode(0.01,15).*power(2,15);
debut = FixCode(-1,15).*power(2,15);
fin = FixCode(1,15).*power(2,15);
fin

function y = FixCode(x,N)

%   Inputs: 
%       x: input data 
%       N: number of bits for the fractional part
%
%   Output
%       y : quantized data with N bits for the fractional part



y = (round(x.*power(2,N)).*power(2,-N));
end

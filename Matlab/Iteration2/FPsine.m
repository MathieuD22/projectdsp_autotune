% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    FPsine.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function signal = FPsine(x)

a = 25718;              % 51437 au format Q(16,0,15) et 25718 au format Q(16,1,14) 
b = -20953;             % Q(16,0,15)
c = 18276;              % 2285 au format Q(16,0,15) et 18276 au format Q(16,-3,18)

xbarre=shift_left(FixCode(x*2/pi,15),15);            %Q(16,0,15)

XbarreP2 = shift_left(multiplier(xbarre,xbarre),1);    %Q(16,0,15)
XbarreP3 = shift_left(multiplier(XbarreP2,xbarre),1);  %Q(16,0,15)
XbarreP5 = shift_left(multiplier(XbarreP3,XbarreP2),1); %Q(16,0,15)

Axbarre = multiplier(a,xbarre);                     % Q(16,2,13) common format
Bxbarre = shift_right(multiplier(b,XbarreP3),1);    % Q(16,2,13) common format
Cxbarre = shift_right(multiplier(c,XbarreP5),4);    % Q(16,-2,17) >> Q(16,2,13) common format

ADD1 = add_int16_int16(Axbarre,Bxbarre);                %Q(16,2,13)
signal = shift_left(add_int16_int16(ADD1,Cxbarre),2);   %Q(16,2,13) >> Q(16,0,15)

end
% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    GenerationSignal.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [signal] = GenerationSignal(TempsDebut, TempsFin, Frequence)
    duree = 5;
    fs = 8000;

    signal = zeros(1,40000);        %fs*duree
    Ts = 1/fs;                      % Periode du signal
    Debut = TempsDebut*40000/duree;
    Fin = TempsFin*40000/duree;
    Temps = TempsFin - TempsDebut;
    
    for i=Debut:1:Fin-1
        signal(i+1) = sin(2*pi*Frequence*(i*Ts));
    end
    
end



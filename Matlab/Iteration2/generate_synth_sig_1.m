% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.

clearvars all 
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    generate_synth_sig_1.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% T/F grid of the first signal to synthetize


Sig1 = GenerationSignal(0,0.5,400);
Sig2 = GenerationSignal(0.5,3,2500);
Sig3 = GenerationSignal(3,4,1200);
Sig4 = GenerationSignal(4,5,400);

signal1 = zeros(1,40000);
for i=1:1:40000
    signal1(i) = Sig1(i) + Sig2(i) + Sig3(i) + Sig4(i);
end

K=100;

AfficheSignalTempsFrequence(8000,signal1)
%plot(signal);
% soundsc(signal)

figure(1)
apodis = blackman(400);             % Création de la fenêtre d'apodisation
matricet = reshape((signal1(1:40000)),[],K);
matricet = apodis.*matricet;
matricef = 20*log10(abs(fft(matricet)));        % Matrice fréqencielle 

imagesc(matricef(1:end/2,:))
colorbar
colormap("default")
set(gca,"YDir",'normal')
title("TF grid for the first signal to reconstruct");
xlabel("Time [s]");
ylabel("Frequency [Hz]"); 
hold on;


% --- UART Test 
% This script aims to create a communication channel between the Zybo board
% and the remote Matlab session on a PC. The purpose is to gather some data
% from the Zybo internal memory that can be interpreted in Matlab for
% debugging purpose.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                    fixSine.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [y] = fixSine(signal, FreqEch, duree)
    
    y = zeros(1,FreqEch*duree);
    a = 4*(3/pi-9/16);
    b = -2*a+5/2;
    c = a-3/2;
    
    for i=1:1:(FreqEch*duree)
        xbarre = (2*signal(i)/pi);
        y(i) = a*xbarre+b*xbarre^3 + c*xbarre^5;
    end
end

















